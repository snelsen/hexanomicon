 export   

![](coversketcho.svg)

# The Void

There was neither aught nor naught, nor air, nor sky beyond. What covered all? Where rested all? In watery gulf profound? Nor death was then, nor deathlessness, nor change of night and day. The One breathed calmly, self-sustained nought else beyond it lay.

Gloom, hid in gloom, existed first—one sea, eluding view. That One, a void in chaos wrapt, by inward fervour grew. Within it first arose desire, the primal germ of mind, Which nothing with existence links, as sages searching find.

The kindling ray that shot across the dark and drear abyss— Was it beneath? or high aloft? What bard can answer this? There fecundating powers were found, and mighty forces strove— A self-supporting mass beneath, and energy above.

Who knows, who ever told, from whence this vast creation rose? No gods had then been born—who then can e’er the truth disclose? Whence sprang this world, and whether framed by hand divine or no— Its lord in heaven alone can tell, if even he can show.

_The Rig Veda_  
Tranlated by John Muir

Judges of this world have little time to concern themselves with its contents. There are no great cities or wizards towers until the adventurers draw them in. The contents of hexes appear there only to serve our tale. They may be called upon as needed to provide impact to a difficult journey. They may provide choices. Do we take the long, and easy road? Or do we cut through _The Glowing bogs of Quirdoon_? This is not a setting, it is a set of tools to create a world.

Rather than a particular place that your campaign inhabits, this world is intended to provide context to and connections between the many great adventures that are available. rather than great journeys, the area immediately around the home village is considered far off land, ripe for exploration. To that end, there are no cities or kingdoms, no histories or preset locales. All that can be developed as the players progress.

These rules should be used only as needed. Rather than a fantasy world simulator, this is more of a fantasy literature simulator. If the party is making a short journey through safe and known territory, simply count off the days. On the other hand, if they are taking a shortcut through the _Fire Swamp_, then an encounter with its vile denizens is assured. If they must make a perilous journey to a far off destination, then they will face beasts, and cold, and more.

Players could start their adventure in a hex next to their village and became _Sailors on the Starless Sea_. They may proceed home from there only to find their beloved village of _Homlit_ under attack by a strange hound in _Doom of the Savage Kings_. Next they journey to other nearby adventures using a hex map that is created as they travel. The hexes can be mundane or mysterious. Areas can have names and contain interesting features and creatures but only if it helps the story. The bandits may be hiding among **The Belching Crystals of Dagoor** or the party may need to take a shortcut through **The Spore Wolves of the Balax Wastes**.

# 1\. Cartomancy

Determine the starting hex. This may be the home village and the surrounding six hexes of fields and hills or forests. You can also fill in hexes for the funnel location, the path there, and an surrounding hexes that may be visible by line of site. Seeing out of swamps or forests is unlikely. Distant mountain hexes may be visible.

## Hex Alignment

A hex may be the domain of evil and chaos. This usually reflected in the atmosphere and features of the landscape but can also be applied to the conditions, the ability to get lost, and the quantity and types of encounters. The party's actions can cause hexes to change alignment. Clearing evil from the land can make journeys safer. Dark forces could also work in the background. Causing evil to encroach ever closer. Other hexes may be the domain of goodness and law but they seem to be fewer and far between in these troubled times.

## The Process

1.  The Players choose their direction of travel. Some or all of the surrounding hex contents may already be visible. Distant mountains may also be visible a few hexes off.
    
2.  Determine if the terrain is impassable or if there are rivers flowing between the hexes. You may also choose to have roads leading through or between hexes.
    
3.  Determine the alignment of new hex.
    
4.  Determine the terrain.
    
5.  Fill in some nearby hexes with the same terrain (all of these hexes have the same effects\*\*
    
6.  Name the area if needed.
    
7.  Mark days off the calendar.
    
8.  Determine the effects of the new hex and execute them.
    
    1.  If there is an environmental effect, execute it.
    2.  If the hex contains beasts, determine the beasts. These beasts will always inhabit this area.
    3.  If the hex is neutral, you may choose to roll for a random encounter.

## Impassable Terrain

After the players choose a direction of travel, and they are not following a road, they may discover that the path forward is impassable. They remain in the same hex and all of that hex's effects are reapplied. Impassable terrain may be predetermined by the judge in order to make an area or an objective more difficult to reach or it may be determined randomly. Lawful hexes are exceedingly rare and sometimes completely surrounded by impassable terrain.

### Table 1-1: Impassable Terrain

Hex

 

 

Hex

 

Neutral

1 on d8

 

Mountanous

\-d1

Chaotic

1 on d7

 

Connected border impassable

\-d3

## Determine the Alignment of the new hex

Base the alingment of the hex either on the requirements of the story or luck. If, in the bard's version of this story, the journey would not warrant even a mention, then it will be neutrally aligned. If they are heading to some goal or retreiving a treasure then it will be chaotic or worse. Any journey of worth will require crossing dangerous ground but you may deem to let the table below decide.

### Table 1-2: Hex Alignment

Possibly modified by luck or amount of luck burned.

d20

Hex Alignment

Fumble

Doom

1-11

Chaotic

12-20

Neutral

Crit

Lawful

## Determine the Topography

The choice may be limited by the die you choose to roll with. Using a d6, d8, or d10 will allow for different possible results.

### Table 1-3: Topography

Roll

Landscape

1

Doom Hex or Plains / hills

2-4

Plains / hills

5-6

Forest / Jungle

7

Mountain Range

8

Desert / Ice

9

Swamp / Bogs

10

Lake or Sea

Fill in some nearby hexes with the same content or use impassable terrain to completely or partially surround goals or show cliffs and canyons. Large rivers are also drawn between hexes and present some difficulty for the party to cross. Possibly the cost is days as the party builds a raft or looks for some other means to cross. You can fill in more hexes if it will be a long and arduous journey but a hex is not a set distance.  
River names: _Course, Estuary, Kill, River, Run, Stream, Tributary, Vein_.

## Time

Mark off the number of days it takes to traverse the hex on your calendar. Plains and hills 1 day, forest 2 days, swamp and desert 3 days, jungle 5 days, mountains 8 days. Chaos and Doom hexes May take half a day or a weeks to cross. Notes these on your map key.

## Provisions and Supplies

You may wish to keep track of supplies. This can work with an inventory slot system of managing encumbrance. One option is to have a single supply unit for both and food and water. The party is assumed to be replenishing supplies as they travel unless they are in hostile (chaotic) terrain. If in hostile terrain they should cross off one supply unit per day. Anyone out of supplies no longer heals. Additionally, if undergoing thirst, exposure, or sickness each member can cross off one supply unit to add 5 to their thirst, exposure, or sickness roll. Similarly, running out of supplies may add to these rolls.

![](image/mapscan.jpg)

# 2\. Chaos

Chaotic terrain can have a variety of effects. These effects may be ameliorated or enhanced by many factors. The characters may carry extra water or have no water at all. Some of the party may have outdoor or astrological skills or even, a compass. You can add modifiers to the roll or move up or down the dice chain. If the party rolls poorly, they may be able to use magic or ingenuity to limit or negate some effects.

## Determine the chaos effect

### Table 2-1: Chaotic Hex Type

d20

result

Fumble

reroll d8+1 and add beasts

2-3

Sickness

4-5

Thirst or Exposure

6-7

Lost

8-10

Storm

11-19

Beasts

Crit

Helpful NPC or traveling merchant

### Beasts

Beasts are the most common trial of a chaos hex. Create your beasts using the _Beastomatic_ below. Make a note of the beast as that same beast will always inhabit this hex. The beast usually attack all who pass through this hex.

### Table 2-2: Sickness

_You feel cold but begin to sweat. Your stomach gurgles. Did you eat something bad?_

d20

Result

Fumble

Pestilence - 1 week and Luck check; Pass: 1d3hp and 1d5 Stamina (cannot go below 1 of either), Fail: Reduced to 1hp and 4 Stamina.

1-5

Plague - 3 day and DC 15 Fort save or 1d3hp and 1d5 Stamina (cannot go below 1 of either).

6-10

Sickness - 2 days and As above but DC 10

11-15

Fevers - 1 day and As above but DC 5

16+

The Runs - 1 day

Crit

Develop an immunity to this area's disease.

### Table 2-3: Thirst

_First begins the terrible thirst then the aching head. The sun beats down as you squeeze out the last drops of water. You tongue swells and your mind becomes haunted._

d20+luck

Result

Fumble

As above and unluckiest succumbs. Permanently lose 1 Stamina and bedridden for 1 week.

\-5

1d4hp+1, 2 Stamina

6-10

1d4hp, 1 Stamina

11-15

1d3hp

16+

Desperate, you find a watering hole

Crit

Healing oasis - Food, water, and all healed

### Table 2-4: Exposure

_You are shivering. Your speech slurs. Clumsy, drowsy you must find shelter._

d20

Result

Fumble

3 Stamina and 3 days + 1d4 Damage, unluckiest gets frostbite, permanently lose 1 Agility.

1-5

3 Stamina & 3 days

6-10

2 Stamina & 2 days

11-15

1 Stamina & 1 day

16+

Find shelter easily

Crit

Survivalist +2XP, Each player acquires warm furs (+3 on this table if they keep and wear)

### Table 2-5: Lost

_The area begins to look familiar. You come upon a campsite and realize it was your own._

d20

Result

Fumble

Lose 1 week, roll on exposure or thirst, encounter beasts.

1-5

Lose 3 days suffer 1d3hp exposure and luck check or encounter a beast.

6-10

Lose 2 days and luck check or encounter a beast.

11-15

Lose 1 day.

16+

Find your way again and lose no time.

Crit

Catch site of and move towards your goal. Gain 1 day.

### Table 2-6: Storm

_The sky grows black and the wind wails._

d20

Result

Fumble

Maelstrom - Lose 4 days, 1d5hp

1-5

Hurricane - Lose 2 day, Each PC makes a Luck check or 1d5hp and 1 Stamina.

6-10

Torrent - Lose 1 day, Luck check or 1d3hp.

11-15

Storm - Lose 1 day

16+

You are very wet

Crit

Searching for shelter, you find food or helpful NPCs.

### Table 2-7: Doom Hex effects

The doom hex represents a particularly dire locale. These areas usually are a lone hex but on occasion may surround an important location. Such a hex could have a particularly vile beast or a dangerous combination of flora and fauna. They could also be combined with the effects below.

d5

Name

Effect

1

Lost time

The days begin to blur. Lose: 1. d3 years, 2-4. d12 months, 5-8. d4 weeks, 9-10. d7 days

2

Never ending

The goal moves. Leaving the hex reenters the hex from the opposite edge. Redoing in hex effects again and again. Reverse course to move forward.

3

Heavenly

It feels too good. The hex gives joy and energy. Delicious fruits and warm springs abound. You feel stronger. The players gain one random point of attribute for each day they spend here. They lose all upon leaving and gain mutations. 5 points gained (and lost) bestows a minor mutation, 10 points gives a major mutation, and 15 points gives greater mutation.

4

Cursed

You despoil hallowed ground by being here. Curse _DCC RPG_ Appendix C. Maybe removed or avoided by a variety of means. Possibly returning an object to the area or killing something.

5

Wasting

Some force drains you. Drains one Strength, Stamina, or Intelligence per day.

![](image/fangsofdorsoom.svg)

# 3\. Names

### Neutral Hex

(_Area Name_) + _Proper Name_  
Examples: _Klazdoon, The Sands of Dagbala, Adaakka Desert_

#### Chaotic Hex

_Special Name A_ + _Area Name_ + (_Proper Name_)  
Examples: _The Blighted Crags, Crooked Woods_

#### Doom Hex

d100 _Special Name A_ + _Area Name Column_ + d10-30 _Area Name_ + (_Proper Name_)  
Examples: _The Womb of Sadness_, _The Screaming Pits of Uruksoom_

## Name Tables

### Table 3-1: Area Names

Choose the correct column for the hex or choose _Other_.

d

Plains/Hills

Desert/Ice

Mountain

Forest

Swamp

Other

1

Plains

Basin

Aerie

Cave

Swamp

Abyss

2

Hills

Desert

Bluffs

Cover

Bogs

Altar

3

Fields

Dunes

Canyon

Forest

Pools

Womb

4

Grasslands

Ocean

Cliffs

Jungle

Mire

Asylum

5

Expanse

Sands

Crags

Shadow

Morass

Bastion

6

Platue

Wastes

Crater

Darkness

Moor

Belly

7

Belt

Salt

Crown

Stand

Fen

Crucible

8

Highlands

Sea

Dome

Thicket

Mud

Dens

9

Border

Blight

Eminence

Trees

Pits

Dwelling

10

Claim

Wasteland

Fjords

Wildwood

Marshland

Expanse

11

Domain

Emptiness

Heights

Wood

Quagmire

Eyes

12

Zone

Lake

Hills

Bramble

Bottoms

Void

13

Flatland

Expanse

Hump

Enclosure

Polder

Remains

14

Footprint

Burning

Mounds

Wildness

Quag

Forge

15

Lands

Desolation

Mount

Silvan

Slough

Garden

16

Lowlands

Glittering

Mountains

Whitewood

Swale

Gorges

17

The Open

Rock

Peaks

Labyrinth

Swampland

Hatcheries

18

Terrain

Wound

Pike

Tangle

Holm

End

19

Territory

Emptiness

Pillars

Boscage

Peat Bog

Corpse

20

The Wild

Anvil

Precipice

Morass

Marsh

Hive

21

 

 

Range

 

 

Lands

22

 

 

Ridges

 

 

Oasis

23

 

 

Spikes

 

 

Pits

24

 

 

Spires

 

 

Plains

25

 

 

Teeth

 

 

Pools

26

 

 

Towers

 

 

Respite

27

 

 

Vault

 

 

Rivers

28

 

 

Volcano

 

 

Sanctuary

29

 

 

Ascent

 

 

Sea

30

 

 

Scar

 

 

Spikes

### Table 3-2: Proper Name

Roll for prefix and suffix. Roll again for additional syllables.

d100

Prefix

Suffix

d100

Prefix

Suffix

d100

Prefix

Suffix

1

Ada

aka

35

Ioni

giss

68

Ras

pur

2

Aksha

alu

36

Ir

glam

69

Roon

queb

3

Awa

alt

37

Isi

glar

70

Sho

quell

4

ba

ax

38

Jai

gloom

71

Shool

quern

5

Bab

balt

39

Jod

hex

72

Shuru

rak

6

Bad

bat

40

Ka

inex

73

Sire

rax

7

Bala

char

41

Kaza

ish

74

Slo

reen

8

Bar

churak

42

Kid

istan

75

Slum

rez

9

Black

dark

43

Klaz

jan

76

Sus

sa

10

Borah

dook

44

Kon

jekto

77

Tak

shul

11

Da

aria

45

Kuta

kan

78

Tal

sippa

12

Dag

bala

46

Kuth

kill

79

Thule

slog

13

Dil

bane

47

La

kkam

80

Tuk

sloth

14

Doon

boon

48

Lag

klor

81

Tut

sogoth

15

Dor

chult

49

Lar

kurnix

82

Tyre

soom

16

Dun

dar

50

Ma

lan

83

Ug

soth

17

D’

dax

51

Mara

lar

84

Uli

sty

18

Ek

doon

52

Mir

lin

85

Ur

sylvania

19

El

down

53

Mont

loloo

86

Uruk

tear

20

Erid

dur

54

Nadi

mar

87

Vek

ton

21

Fal

ech

55

Nag

mare

88

Ver

tooth

22

Far

eek

56

Nin

mexy

89

Vil

twool

23

Fe

eel

57

Noog

mire

90

Vin

usix

24

Fell

eno

58

Og

nadir

91

Walden

vex

25

Fy

entix

59

Oog

neen

92

Wil

vile

26

Gar

ereen

60

Ool

nipe

93

Xen

wan

27

Gir

eria

61

Oor

noop

94

Xi

weird

28

Glam

fall

62

Ozy

nun

95

Xyl

wood

29

Glom

fell

63

Pazur

oop

96

Yi

xene

30

Gun

fex

64

Py

ount

97

Yool

xyl

31

Hi

foor

65

Quir

ox

98

Zen

zalu

32

Hun

gal

66

Ra

oz

99

Zeph

zoon

33

Idle

gar

67

Ral

por

100

Zir

zzarr

34

Ill

gex

 

 

 

 

 

 

![](image/skull_island.svg)

### Table 3-3: Special Hex

d100

Title

d100

Title

d100

Title

1

Abhorrent

35

Heretical

68

Unseen

2

Belching

36

Hidden

69

Unspeakable

3

Gloaming

37

Horror

70

Wasted

4

Bent

38

Ice

71

Weeping

5

Black

39

Iron

72

Whispering

6

Blasphemous

40

Lava

73

White

7

Blessed

41

Living

74

Wounded

8

Blighted

42

Maw

75

Screaming

9

Blood

43

Metal

76

Great

10

Bogs

44

Moldy

77

Dire

11

Burn

45

Mud

78

Fearsome

12

Cloud

46

Obscene

79

Feeding

13

Corrupted

47

Pain

80

Filthy

14

Crooked

48

Poison

81

Sunken

15

Cruel

49

Red

82

Sordid

16

Crystal

50

Rot

83

Milky

17

Croaking

51

Rotting

84

Pustulent

18

Cursed

52

Mourning

85

Impurpled

19

Dark

53

Ruin

86

Corpulant

20

Dead

54

Sadness

87

Saloubriuos

21

Deformed

55

Salt

88

Salivating

22

Doom

56

Shadowed

89

Sinful

23

Dreaming

57

Shifting

90

Painted

24

Fear

58

Shroomed

91

Steel

25

Fire

59

Demonic

92

Bleached

26

Fiery

60

Shroud

93

Baneful

27

Floating

61

Smoldering

94

Billowing

28

Forgetful

62

Sorrow

95

Whistling

29

Formless

63

Spiked

96

Whispering

30

Frozen

64

Tangled

97

Slumping

31

Ghost

65

Unbidden

98

Slouching

32

Glittering

66

Unborn

99

Polluted

33

Glowing

67

Unholy

100

Ashen

34

Grey

 

 

 

 

![](image/GlowRatSplat.svg) ![](image/GlowRat.svg)

Glowing Slime Beast

_Rolls: Glowing, Slime, Bear_

Init (always last); Atk bite +2 melee (1d6 + grapple) and claw +4 melee (1d4+1)+ shove; AC 13; HD 3d10; HP 17; MV 40'; Act 1d25(crit 23-24), 1d16 claw; SP maul, grapple, shove; SV Fort +5, Reflex -3, Will -4; AL N. Maul for additional an 1d8 damage if bite victim remains grappled next turn, claw auto hits and crits on 16. Shove: If not grappled and claw causes 4 or more damage, victim is prone.

Enveloped in green fire. Infravision is blinded in darkness, -1 Stamina per round to all within melee range. Losing 3 points causes vomiting. Reduced to 3 Stamina causes mutation. _Acidic Touch_: additional 1d6 damage.

![](image/GlowRatSplat.svg)

# 4\. Beastomatic

Roll one or two items from the following two lists to create creatures like; **Burrowing Slime Women** or **Spitting Rat Monkeys.** There are alternate names as well. Some items could provide only flavor while others may be used, but not be part of the creatures proper name. Additional names can be found in DCC RPG Appendix S.

The powers from the first table applied to the base creatures on the second table. _Acid Dogs_ are easier to stat than _Pig Wolves._ You may interpret the names so that _Magic_, _King_, _Zombie_ and _Flower_ could become _Lich Orchid_. _Flying_ or _Bird_ and _Un-dead_ could be _Flying_ _Skulls_.

### Table 4-1: Beast Power

Name, Special, alternate names.

1.  **Acid** Damaging this creature sprays 1d4 damage for 1d3 rounds unless washed off, plate armor may protect against this damage while non-metal armors may be damaged; +3 Fort.
    
    Caustic, Burning.
    
2.  **Ambush** Surprise round, +3 AC, +5 stealth/hide.
    
    Shock, Sneak, Camouflage, Chameleon
    
3.  **Beast** Muscular and hairy or deformed, such as faces on torso and no head, +1 HD, +1 DMG, +1 Init, +2 Fort, +1 Ref, -2 Will.
    
    Monster, Monstrous, Fiend, Ferrel, Wild, Mishapen, Demented
    
4.  **Blinding** Spit or glowing eyes attack can cause temporary blindness, +2 ranged 30', 1d3 rounds of blindness, extra 1d16 for blinding attack, blinding attack ignores armor.
    
    Shining, Blazing, Spit
    
5.  **Blood** If an attack causes damages all beasts target wounded character with +2 and new beasts may appear.
    
6.  **Brain** Intelligent, possibly psionic. +3 Init, Psionic attack DC 15 Will save or 1d4 damage and lose a turn, +5 Will.
    
7.  **Burrowing** Can come up anywhere with surprise, +2 Init, may come up in waves. If paired with other words like _Brain Burrowing,_ Use something like grapple on hit and -1 Intelligence damage.
    
    Mole
    
8.  **Claw** Giant claws. +3 DMG or +d16 Act claw attack, grapple on hit
    
9.  **Corrupting** Successful attack causes a mutation in 2d4+Stamina days unless magically healed. Based on damage; 1-12: minor corruption, 12-20: major corrption, 21+: greater corruption (or DCC Annual mutation table).
    
    Radioactive, Mutator, Mutated
    
10.  **Crusher** Massive mandibles. Grapple causes 1d6 DMG next turn.
    
    Grapple, Smasher, Hammer
    
11.  **Jumping** Jump 40', +1 Init, +4 Ref. Can leap over opponents.
    
    Leaping, Hopping
    
12.  **Death** Only one assailant but death attack causes 2d10 Stamina drain at 2 Stanima/round. Creature fades away after a succesful attack.
    
    Dark
    
13.  **Demon** Red with full black eyes. Attack causes Fort save (DC based on attack roll) or paralysis (-d3 , crawl 5', -5 AC, Halflings are immune), +2 Init, +2 Fort, +1 Ref, +3 Will.
    
    Imp, Demonic, Unholy, Cursed, Debased,
    
14.  **Doom** Black spikes or flails or blade armor, +2 Damage, Deed die +d1/HD so a 1HD creature has d3 added to action and damage.
    
    Black, Spike, Flail, Impalor
    
15.  **Dream** DC 10 Will save or lose a round, every round. +8 DC if you missed your save last round. Nod out. Elves are immune.
    
    Euphoria, Soma, Ambrosia, Poppy, Peyote
    
16.  **Electro** Attacks cause additional 1d4 electrical damage to target and all within 5' of anyone who receives electrical damage or all in water. Any melee attack with metal that hits must DC 10 Fort save to hold on to weapon.
    
    Shock, Shocking, Electric, Zap
    
17.  **Evil** Fights to death. +3 Init; Act +1d16; +3 Ref; Alignment Chaotic.
    
    Vicious, Berserker
    
18.  **Extra Bits**
    
    d8
    
    Extra bit
    
    Effect
    
    1-2
    
    Roll twice more
    
    These can stack so that you can get all results if you keep hitting 1 or 2
    
    3
    
    Head
    
    +1d20 bite if it bites, Init +2, +3 Will, -50% chance to be surprised, +5 Will
    
    4
    
    Arms
    
    +1d20
    
    5
    
    Legs
    
    +20' move
    
    6
    
    Tail
    
    +1d20 tail attack (1d5 damage)
    
    7
    
    Quills/spikes
    
    +d1 damage to attack and/or grapple
    
    8
    
    Scales/shell
    
    +4 AC
    
19.  **Fade** Fade to invisible; Melee +2, DC 15 INT check to see it or attack at -d2.  
    You see only a disturbance in the light and footprints created.
    
    Invisible, Inviso, Ghostly
    
20.  **Fire** Napalm spray. +3 ranged 20’, catch fire. 1d4 each round until you put it out.
    
    Burning, Napalm, Fire Breathing, Fiery
    
21.  **Fear** Morale check for hirelings and 0-levels. DC 20 Will Save or and spell check -d1.
    
22.  **Flying** Fly 50'. May have wings, +2 AC and fewer HD or hp.
    
23.  **Fog** A shroud of fog precedes and surrounds them but does not affect them. Can only be melee attacked and at -1d.
    
    Mist, Shroud
    
24.  **Freeze** Breath weapon, DC 15 Ref or touch. Lose 10’ speed, -2 to attack, 1d4 cold damage. Modifiers last 3 rounds and stack on succesive hits.
    
    Alternate: Aura 10'. Lose 5' speed, -1 to attack, 1d3-1 cold damage. Modifiers stack each round for 3 rounds.
    
25.  **Gas** Poison cloud. May require recharge. 30’ area, centered 15' from attacker and lasts 3 rounds.
    
    1\. Poison 1d4/turn; 2. Sleep (DC 10 WILL save each round of exposure); 3. Dizzy -d1 actions
26.  **Giant** +50% HD, +3 AC, +d2 DMG, Crit on 19-20
    
    Gargantuan, Humongous, Gigantic, Huge, Towering
    
27.  **Glam** Illusory appearance. Roll on NPC table or make them beautiful or cute.
    
    Glamorouse, Beautiful, Lovely, Cute, Helpless
    
28.  **Glow** Enveloped in green fire. Infravision is blinded in darkness, -1 Stamina per round to all within melee range. Losing 3 points causes vomiting. Reduced to 3 Stamina causes mutation.
    
    Radioactive, Corrupting
    
29.  **Gore** Additional 1d20, Piercing tentacles, +3 melee, 1d4 DAM + grapple & 1d6 DAM/turn until DC 15 STR or tentacles are cut.
    
    Tentacle
    
30.  **Healer** -1 AC, +1 HD, +3 Fort.  
    This applies to only one of a group. _Laser Slugs_ would have one _Healer Slug_ with them. d3: 1. Auto area: 10' radius, 2hp/round. 2. Area: 5' radius, 4hp/round as an action. 3. Targeted: heal 1 creature for 2 HD as an action.
    
    Cleric, Voodoo, Witch Doctor, Shaman
    
31.  **Horror** +2 AC, +3 Init, Surprise and disappear in darkness (DC 20 to see or DC 15 with infravision). Humans must DC 15 Will save each round until success or stupor.
    
32.  **King** +2 Init, +1 AC, +1 HD, MV +20. Only one but has minions which receive +d1 , +5 Will save, pass morale checks while present and fail morale when not. Minions can be normal versions of the same creature or other creatures rolled only on table 4-2.
    
    Chief, Chieftan Boss, Leader
    
33.  **Lascivious:** You feel something stir inside you.  
    DC 15 Will save or attempt to give it a hug and must pass DC 10 Will for every attempted attack. Elves are immune.
    
    Lovely, Lonely
    
34.  **Laser** Burning rays. 60' ranged (1d4+1).
    
    Ray, Beam
    
35.  **Mad** -1 AC, -1 Init, If a melee attack hits it attacks another target in melee range. Continues until miss.
    
    Insane, Berserker
    
36.  **Magic** These creatures may have innate magical abilities or they have abilities similar to magical abilities.  
    Spells other than _anti-magic_ have a 50% chance of success and are lost on failure.
    
    d10
    
    Spell
    
    Spell Check
    
    1
    
    Roll twice more
    
     
    
    2
    
    Chill touch pg. 133
    
    SC 17
    
    3
    
    Chocking cloud pg. 134
    
    SC 13
    
    4
    
    Color spray pg. 135
    
    SC 13
    
    5
    
    Flaming hands pg. 142
    
    SC 17
    
    6
    
    Magic missile pg. 144
    
    SC 13
    
    7
    
    Magic shield pg. 146
    
    SC 13
    
    8
    
    Sleep pg. 156
    
    SC 13
    
    9
    
    Phantasm pg. 187
    
    Illusion of larger creature or NPC
    
    10
    
    Anti-magic
    
    Blocks all magic and negates bonus from magical items
    
    Wizard, Sorcerer, Arcane Additional names per spell's name or manifestation.
    
37.  **Magic Drain** Magical weapons that hit this creature heal instead of damage and lose their magic until the next day. Many spells have no effect and spells that cause damage heal. May even add HD and increase in size.
    
38.  **Mind** What are these whispers in my head?  
    DC 15 Will save or attack a friend at -2 melee until passing Will save.
    
    Mind Control, Psychic, Psionic
    
39.  **Pain** Attack causes -1d and -5 Reflex until any healing.
    
40.  **Phase** You see them and they fade away only to reappear closer. Phases in and out of reality. +4 AC, Melee attacks DC 15 FORT save or stun for 1 round and ignore armor/shield except magical. +6 Reflex
    
    Spectral, Warp, Displacer
    
41.  **Plague** Bleeding sores and foaming mouth, weeping fleas engulf you.  
    Everyone in melee range must make a DC 15 FORT or become ill. d3: 1. Dwarves, 2. Elves, 3. Halflings, are immune. Lose 1 Stamina/day for 1d20 days (judge may roll in secret). Act -1d/day after 3 days. Bedridden and require care below 6 Stamina (-3 Stamina/day without care). Death at 0 Stamina. Anyone caring for you must pass DC 10 Fort save or become ill as well. These crazed creatures always pass morale checks.
    
    Infectious, Sickening, Pestilent, Calamity, Rabid
    
42.  **Poison** Method (if needed) d3: 1. claw or bite or weapon, melee; 2. spray or gas, 10'x10' centered 5' away, DC 15 Ref save; 3. spit, ranged 30'; Resistance d6: 1. None 2. Dwarves 3. Elves 4. Halflings 5. Human females 6. Thieves
    
    d8
    
    Fort
    
    Pass
    
    Fail
    
    Name
    
    1
    
    10
    
    1 Stamina
    
    1d6 Stamina and 1d3hp
    
    Draining, Wasting
    
    2
    
    10
    
    1 Agility
    
    1d6 Agility
    
    Tremor
    
    3
    
    10
    
    1 Intelligence
    
    1d8 Intelligence
    
    Stupefying, Stunning
    
    4
    
    10
    
    \-1 Strength
    
    \-2d3 Strength
    
    Weakness
    
    5
    
    5
    
    Blind 1d3 rounds
    
    Blind (permanent)
    
    Blinding
    
    6
    
    15
    
    No effect
    
    Paralyzed 1d3 rounds
    
    Paralyzing
    
    7
    
    12
    
    1d4 damage
    
    1d3 damage/round for 1d6 rounds
    
    Wasting
    
    8
    
    6
    
    No effect
    
    2d8 damage (if not reduced to less than 0hp, permanent +1 stamina after fully healing)
    
     
    
    Black, Widow, Death, Viper for more names see individual poison type.
    
43.  **Puss** Skin taught over bloated body.  
    Explodes when killed causing 1d4 in a 5’ radius. Can cause a chain reaction.
    
    Exploding, Bursting, Burst
    
44.  **Ram** Charge attack: +2 to hit and damage, AC -2 until next round, must move at least 5', targets who take more than 3 hp damage must DC 10 Ref save or prone.
    
    Rush, Push, Slam, Bash, Charging
    
45.  **Rune** Glowing runes. Regenerates 2 hp every round, as an action it can regenerate 1 HD.
    
46.  **Sleep** Song or smell causes sleep (DC10 WILL). Elves are immune.
    
47.  **Slime** Slime beasts are creatures that have been assimilated by primeval slime monsters and retain some of the creatures behaviors. They appear as a zombie or skeleton enveloped in a slime body. Roll for 1d4-1 ooze special properties on DCC RPG Core Pg. 424. Init (always last); HD +d1; MV -5', Climb; SP Un-dead traits and ooze; Fort +4, Ref -4, Will -2; AL N.
    
    Goop, Glue, Gelatinous, Gel, Blob, Jelly
    
48.  **Slow** Attack or area effect causes -10’ Speed, -3 Init, -4 Reflex for 1d10 turns
    
49.  **Snipe** Shoots darts or quills from up to 90'. May have limited ammo or require recharge.
    
    Sniper, Missile
    
50.  **Speed** +20’ move, +3 Init, +4 Reflex
    
    Hyper
    
51.  **Spider** +20’ move climb, 30' jump, +3 Init, may shoot sticky web (see below).
    
52.  **Spitting** 30’, DC 15 Ref save, 1. Poison (see above) 2. Acid (see above) 3. sticky (-d1 ) 4. Attractant (summon more) 5. Stink (lose a round and stink for d16 days) 6. Drug
    
53.  **Spore** releases cloud of spores from holes in body. -1 on all rolls from choking or obscured by cloud. Humans are allergic, DC 15 Fort save or lose a turn.
    
54.  **Swarm** Smaller or normal size version as a single swarm. Multiply quantity by hp to get total hp. All in 40' wide circle are attacked each round for half damage. DC 12 Strength check or non-flying creatures drag you down for -2 AC and -d1 . DC 12 Will save against flying creatures or become disoriented for -2d next turn (halflings are immune). Attacks that are not area effect do half damage.
    
55.  **Vampire** +3 Init, +3 Bite (1d4) auto grapples (1d3hp/round)
    
    Blood Sucking, Blood Sucker, Leech
    
56.  **Vile** DC 15 Fort save all within melee range or wretch 1 turn. Roll each turn until you make the save (Dwarves are immune).
    
    Disgusting, Gross
    
57.  **Wasting** Exudes black aura. You feel weaker. -1d to all actions and -1 Stamina/round for each round in melee range. Does not stack.
    
    Draining, Doom
    
58.  **Web** Shoots a sticky web at a 10' square, DC 15 Reflex save to avoid. Move reduced 15', -1d to attack, +1d to be attacked. DC 15 Strength to break free.
    
59.  **Wraith** Attracted to lucky PCs. Attack does luck damage. Unconscious at 0 Luck. Any one left unconscious with the Wraiths becomes a _Luck Zombie_.
    
60.  **Un-dead** Appearance and traits _DCC RPG core rulebook_ Page 381. +3 Attack, +1 HD, -2 AC, -3 Reflex. Does not eat, drink, breath, or sleep. immune to _sleep_, _charm_, _hold_, mental effects, and cold damage. May have other effects for _Skeleton_, _Mummy_, or _Ghoul_.
    
    Zombie, Skeleton, Grave, Soulless, Undying, Eternal, Ghoul, Mummy, Ghoulish
    

![](image/GlowRatSplat.svg) ![](image/howler.svg)

_Rolls: Evil, Plague, Monkey_

(3d3x2): Init +4, Atk claw/bite (1d4-1), missile 30' ranged (1d3); AC 13; HD 1d6; MV 30' Climb 30'; Act 1d20; SV Fort +3, Ref +7, Will -3; AL C. Devious and mean, monkeys leap from trees to attack. Leaping attacks are +d1. Successful attacks grapple. Grappled opponents -1AC/monkey. Each monkey takes a DC 8 Strength action to remove. DC 18 to remove 4. Attached monkeys attack with +d1. Grappled opponents are targeted by other monkeys but not with missile attacks. A monkey has a 1/4 chance to have a rock if it has not melee attacked. All monkeys have an unlimited supply of scat but it does no damage. No morale checks. Fights to death.

Bleeding sores and foaming mouth, weeping fleas engulf you. Everyone in melee range must make a DC 15 FORT or become ill. Halflings, are immune. Lose 1 Stamina/day for 1d20 days (judge may roll in secret). Act -1d/day after 3 days. Bedridden and require care below 6 Stamina (-3 Stamina/day without care). Death at 0 Stamina. Anyone caring for you must pass DC 10 Fort save or become ill as well.

Evil Plague Monkeys

### Table 4-2 Beast Type

1.  **Ant** (1d8+10): Init +0; Atk bite +2 melee (1d3 plus latch); AC 14; HD 1d4; MV 20’ or climb 40’ (difficult terrain that may slow players allows ants to use their climb speed); Act 1d20; SP Latch; SV Fort +5, Ref +1, Will -3; AL L. Successful attack latches on. -AC and -d1 per ant until dc10 Str to shake one off or dc 20 to shake all off.
    
2.  **Ape** (3d3): Init +1; Atk bite +2 melee (1d4+1) or slam +3 melee (1d6); AC 10; HD 1d8+2; MV 20' or climb 30'; Act 1d20; SP +5 to hide checks in jungle terrain; SV Fort +4, Ref +2; AL N.
    
3.  **Bats, Giant** (3d3): Init +4; Atk bite +3 melee (1d5 + disease); AC 13; HD 1d6+1; MV fly 40’; Act 1d20; disease, no natural healing for 1d14 days; SV Fort +3, Ref +10, Will -2; AL L.
    
4.  **Bear** (1): Init +2; Atk bite +2 melee (1d6 plus grapple) and claw +4 melee (1d4+1)+ shove; AC 13; HD 3d8; MV 40'; Act 1d24 (crit on 23-24), 1d16; SP maul, shove; SV Fort +1, Reflex +1, Will -2; AL N. Maul for additional an 1d8 damage if bite victim remains grappled next turn, claw auto hits and crits on 16. Shove: If not grappled and claw causes 4 or more damage, victim is prone.
    
    Monster, Beast, Giant
    
5.  **Beetles** (2d5+5): Init -2; Atk mandibles (1d3+1); AC 15; HD 1d6; MV 20' or climb 20' or Fly 30'; Act 1d20; SV Fort +1, Ref +0, Will -3; AL N.
    
6.  **Birds, Large** (3d3+3): Init +3; Atk claw +6 melee (1d4); AC 16; HD 1d5; MV fly 40’; Act 1d20; SV Fort +0, Ref +6, Will -2; AL N.  
    Birds appear in small numbers and slowly grow as more join, only attacking in sufficient numbers.
    
    Flock, Murder, Host, Crows
    
7.  **Cats** (2d3): Init +1; Atk claw +2 melee (1d3) or bite +3 melee (1d5); AC 12; HD 1d8; MV 40' or climb 20'; Act 1d20; SP _pounce_; SV Fort +2, Ref +3; AL N; Crit M/d8  
    If the mountain-lion cougar makes the first attack of combat, it will pounce; otherwise it attacks normally. Thereafter, it will alternate attacks between claw and bite, pouncing when possible.  
    _Pounce_: The mountain-lion cougar can pounce to gain an extra d20 attack die and attack that round with both a claw and bite. The mountain-lion cougar can only pounce if it surprises its victims, attacks first due to initiative, or has taken no damage since its previous attack.
    
    Pride, Tigers, Lions, Panthers, Cougars, Jaguars, Kittens
    
8.  **Crabs** (2d3+2): Init +1, Atk 2x +3 Claw (1d4+1); AC 15 (7 on underside); HD 1d8; MV 30'; Act 2d20; SV Fort +4, Ref -1, Will -2.
    
9.  **Centipedes, Large** (4d3): Init +1; Atk bite +6 melee (1d6); AC 14; HD 1d4; MV 40' Climb 40'; Act 1d20; SV Fort -1, Ref +2, Will -1; AL C.
    
10.  **Dogs** (1d6+6): Init +2; Atk bite +4 melee (1d3+1); AC 11; HD 1d5; MV 35'; Act 1d20; SV Fort +2, Ref +3; AL N.
    
    Hounds, Puppies, Mongrels, Mutts, Foxes, Vixens
    
11.  **Elves** (2d4+1): Init +2; Atk weapon +1 melee or +2 ranged; AC 11; HD 1d8; MV 30’; Act 1d20; SP hide; SV Fort -2, Ref +1, Will +4; AL C.  
    Twisted versions of Elves. Immune to magical _charm,_ _sleep,_ ect. Can see through most glamours and illusions. As an action they can hide in their home landscape, DC 20 Int check to spot one. May have crude weapons and crude armor.
    
    Faeries, Sprites, Spirit Folk
    
12.  **Flowers, Giant** (1d3+3): Init -1; Atk leaf slash +3 melee 10' reach (1d4+1); AC 9; HD 1d30; MV 0'; Act 2d24; SP scent attractant, flora; SV Fort +0 but immune to many effects, Ref -5, Will +10; AL C.  
    Giant flowers seem harmless enough until you find yourself in the middle of them. They open their scent sacks beckoning unsuspecting creatures for a taste. Those who succumb walk slowly towards the flower (half speed), pushing away in who would stop them. They dunk their head into the flower and hang their limply until dead (1d3 Stamina/round). DC 15 Will save, victims may get another save if someone tries to help them or there was a distraction as they walk towards the flower.
    
    Flora are mindless and enjoy similar powers to un-dead but are susceptible to critical hits. Flora takes double damage from fire and half damage from peircing or bludgeoning attacks.
    
    **Flowers, Normal** You feel sleepy crossing a 200' field of flowers. 100' feet in requires a DC 7 Will save every 10' or lose 1d3 Int. The DC increases by 2 at each failure. If Int falls below 3, fall asleep. Either be rescued or wake up 12 hours later losing 1 Int permanently. Elves may be immune.
    
    Poppies, Orchids, Roses, Lilies, Lotus, Geraniums
    
13.  **Ghost**: Use stat block or powers or both. (1d4) Init +2; Atk special (see below); AC 10; HD 2d12; MV fly 40’; Act 1d20; SP un-dead traits, immune to non-magical weapons; SV Fort +2, Ref +4, Will +6; AL C.
    
    d4
    
    Power
    
     
    
    1
    
    Psychic Scream
    
    DC 15 Will save or 1d4 Damage.
    
    2
    
    Telekinetic Attack
    
    Objects from random locations, missile +5 Ranged (1d4 damage).
    
    3
    
    Madness
    
    DC 15 Will save of 1d3 to Intelligence and lose a round.
    
    4
    
    Invisibility
    
    Turn invisible as an action and reappears to +5 attack.
    
    Phantasms, Spectres, Spirits, Apparitions, Ghostly
    
14.  **Gnomes** (3d4): Init +1; Attack 2 daggers (1d4 damage) or fists (1 damage); AC 12; HD 1d4; MV 20'; Act 2d16; SP crit, parry, fairy glow; SV Fort +2, Ref +3, Will; AL C.  
    These creatures often rely on magical powers or trickery and traps rather than physical abilities. They usually have more than one of the magical **Beast Powers.** When fighting they crit an any sixteen and fumble only on double 1. Gnomes can disengage from melee as an action. Anyone touched by a gnome gains _Fairy Glow_ and suffers -2 AC for the day or knight.
    
    Kobolds, Leprechauns, Dwarves, Hobbits, Lilliputians, Munchkins, Pixies
    
15.  **Goblins** (1d4+4): Init -1; Atk bite -1 melee (1d3) or as weapon -1 melee; AC 10 + armor; HD 1d6-1; MV 20’; Act 1d20; SP infravision 60’; SV Fort -2, Ref +1, Will -2; AL L. (_DCC RPG_ Pg. 417)
    
16.  **Lizards** (1d3+1): Init +1; Atk bite +1 melee (1d8 + grapple) or tail +3 melee (1d6); AC 16; HD 3d8; MV 20', swim 40', Act 1d24; SP tear; SV Fort +3, Ref +1, Will -4; AL N.  
    Grappled opponents are dragged away next turn and torn apart for 1d10 damage. If used as a **Beast Power** rather than **Beast Type** (i.e. _Lizard Men_), +2 Damage and +2 AC and camouflage (DC 15 Int check to see in natural environment).
    
    Gila Monsters, Alligators, Crocadiles, Gators, Crocs, Dragons, Worms
    
17.  **Men** (4d4): Init -2; Atk club -1 melee (1d4-1) + grapple; AC 10; HD 1d8; MV 30’; Act 1d20; SV Fort -1, Ref -2, Will -1; AL C.  
    May have other simple or stolen weapons and armor.
    
    Thugs, Cannibals
    
18.  **Monkeys** (3d3x2): Init +1, Atk claw/bite (1d4-1), missile 30' ranged (1d3); AC 13; HD 1d6; MV 30' Climb 30'; Act 1d20; SV Fort +3, Ref +4, Will -3; AL N.
    
    Devious and mean, monkeys leap from trees to attack. Leaping attacks are +d1. Successful attacks grapple. Grappled opponents -1AC/monkey. Each monkey takes a DC 8 Strength action to remove. DC 18 to remove 4. Attached monkeys attack with +d1. Grappled opponents are targeted by other monkeys but not with missile attacks. A monkey has a 1/4 chance to have a rock if it has not melee attacked. All monkeys have an unlimited supply of scat but it does no damage. Fire damage causes a morale check.
    
19.  **Plants** (1d6+3): Init -1; Atk slash +0 melee (1d5), vine +1 grapple 15' reach (drag in 5'); AC 14; HD 2d8; MV 0'; Act 1d24 slash, 2d16 vine SV Fort +6; Ref -6, Will -3; AL C.
    
    This evil flora does not present a threat until the party is in the center of a group. Vines reach out to pull victims in to suffer slash attacks. Vines ignore armor.
    
    Flora are mindless and enjoy similar powers to un-dead but are susceptible to critical hits. Flora takes double damage from fire and half damage from peircing or bludgeoning attacks.
    
    Any plant name Cacti, Verbana, Brush, Thistles
    
20.  **Pigs** (Special): Init +2; Atk bite +4 melee (1d3); AC 11; HD 1d5; MV 35'; Act 1d20;SP _Charge_; SV Fort +2, Ref +3; AL N.
    
    Charge attack: +2 to hit and damage, AC -2 until next round, must move at least 5', targets who take more than 3 hp damage must DC 10 Ref save or prone.
    
    Roll d6. Your roll is the amount, the remainder is HD. So a roll of 2 would mean two 4HD pigs. 1HD minimum.
    
    When used as a **Beast Power** (_Zombie Pig Men_) rather than a **Beast Type**, use the **Beast Power:** _Beast_.
    
    Boars, Hogs
    
21.  **Rats, Ususual Size** (1d6+6): Init +4; Atk bite +2 melee (1d4+1 plus disease); AC 13; HD 1d6+2; MV 30’ or climb 20’; Act 1d20; SP disease (DC 7 Fort save or additional 1d6 damage); SV Fort +4, Ref +2, Will -1; AL C.
    
    Vermin, Rodents, Nutria, Moles, Possum, Trash Pandas
    
22.  **Serpents, Constrictor** Init +4; Atk bite +6 melee (1d4 + constrict); AC 14; HD 2d8; MV 30'; Act 1d20; SP constriction 1d5; SV Fort +6, Ref +3, Will +2; AL N.
    
    **Serpents, Poisonous** Init +8; Atk bite +4 melee (1d3 + poison); AC 12; HD 1d8; MV 40'; Act 1d20; SP see _Poison_ above; SV Fort +1, Ref +6, Will +2; AL N.
    
    Snakes, Cobras, Vipers
    
23.  **Slugs** (2d3): Init -6; Atk acidic touch +3 melee (1d4); AC 18; HD 2d6; MV 10’; Act 1d20; SV Fort +5, Ref -6, Will -2; AL C.  
    Slow but covered in slime and leaves a trail which may enhance their _Beast Power_.
    
    _Nudibranch, Leech, Worm, Amoeba_
    
24.  **Spiders, large** (2d4): Init +3; Atk bite +2 (1d3 + poison _see above_); AC 14; HD 3d5; MV 40', climb 40', jump 30'; Act 1d20; SV +4, Ref +8, Will -4; AL C.
    
25.  **Trees** (2d10): Init -3; Atk branch -2 melee (1d4-1); AC 16; HD 3d8+10; MV 0'; Act 4d20; SV Fort +10; Ref -10; Will +10; AL N.
    
    Trees attack with their branches and, usually one must make a fighting escape as you run through them.
    
    Flora are mindless(?) and enjoy similar powers to un-dead but are susceptible to critical hits. Flora takes double damage from fire and half damage from peircing or bludgeoning attacks.
    
26.  **Wolves** (1d6+3): Init +3; Atk bite +2 melee (1d4); AC 12; HD 1d6; MV 40’; Act 1d20; SV Fort +3, Ref +2, Will +1; AL L. (_DCC RPG_ Pg. 431)
    
    Pack, Coyotes, Hyenas, Dingos
    
27.  **Women** (3d3): Init +2; See below; AC 12; HD 1d7; MV 30’; Act 1d20; SV Fort +1, Ref +2, Will +2; AL C. Straight out of the male subconscious. Magic may only affect males.
    
    *   Skin: 1. scales +4 AC, 2. fish scales, 3. Feathers, 4. ink black 4 violet, green, orange, white, normal.
    *   Hair: 1. Snakes (grapple = bite +2 melee, 1d3 + poison DC 15 Fort or 1d5 damage), 2. Tentacles (+4 grapple = free bite, attempting to breaking free inflicts 1d3 damage), 3. Long dreds, 5. Blue, 6. Green 7. Bright red, 8. Black
    *   Eyes: Choose color, insect, goat, frog, cat, solid color, not two, and/or glowing
    *   Other features: Horns, antlers, antennae (insect), antennae (alien)
    
    d5
    
    Weapon
    
    1
    
    Roll twice on Magic table & fangs/claws
    
    2
    
    Fangs/claws, bite +3 melee (1d6 damage) and claw +5 melee (1d4 damage); 2d20
    
    3
    
    Roll once on Magic & Dagger +5 melee (1d4)
    
    4
    
    Poison dagger +5 melee (DC 15 Fort or sleep)
    
    5
    
    Poison dagger (DC 15 Fort save or 1d6 damage)
    
    6
    
    Whip +5 melee, 10' reach (1d6 or DC 15 disarm or DC 15 grapple)
    
    7
    
    Short sword +5 melee (1d6)
    
    Coven, Hags, Witches, Bells, Maidens, Betties, Eves, Lilliths, Mollys, Amazons, Maids, Girls, Dolls, Sallies, Trollops, Hussies, Vixens, Gasts, Harridans
    
28.  **Worms** (1d3x5): Init -1; touch +3 melee (magical or physiological effect or 1d4 damage); AC 12; HD 2d8; MV 30'; SP morphogenesis; SV Fort -2, Ref +1, Will -1; AL N.
    
    Morphogenesis: Any worm cut with a slashing instrument takes the following round to become two worms.
    
    Tape, Flat, Planarium
    
29.  **Mushrooms** (2d4)Init -5; slam +5 melee (1d5 damage); AC 12; HD 4d6; MV 20; SV Fort -2, Reflex -6, Will -4; AL N.
    
    These creatures are found in a field of puffballs. Attacking them requires a DC 10 Ref save or set one off.
    
    Effects of eating fresh mushroom: Roll 1d20+15+Luck on _DCC RPG Pg. 223_ and then random.
    
    **Puffballs** Spore explosion DC 15 Reflex or 1d3 damage and another DC 10 Reflex or you set off another one (continues indefinitely on failure).  
    Halfway through a 200' field of these strange orbs one explodes hitting the unluckiest party member. Point person must weave there way through the orbs for the next 100'. DC 10/round for normal movement, DC 15 for double, and DC 5 for half. All who follow need only a single DC 10 to cross the entire field. d10 to determine where any failures occured. Last person to leave the field must make a luck check or set off a puffball.
    
    Shrooms, Funghi, Fungus, Deathcaps
    
30.  **Vines** Strange vegetation fills this area. Vines attack suddenly but only once. Each character is attacked by 1d8 vines. DC 15 Ref save for each vine. The vines that attack successfully pull taught, opposed Strength with an additional +2 per vine. If a blade is available, a luck check may allow its use. Anyone helping someone caught by vines is attacked by the vines. Torches or any fire source scares vines away.
    
    Creepers
    

### Table 4-3: Weakness or Phobia

d30

Weakness

d30

Weakness

1

Water

16

These violet glowing crystals

2

Fire

17

Those green mushrooms over there

3

Cold

18

Oil

4

Wood

19

Pony, Horse, Mule

5

Elves

20

Honey

6

Halflings

21

Stinky Cheese

7

Dwarves

22

Fruit

8

Women

23

Mithril

9

Gold

24

Falcons

10

Aggressive behavior

25

Hens

11

Magic power

26

Night Soil

12

Light

27

Dolls

13

Dark

28

Dogs

14

Paper

29

Gems

15

Affection

30

Pushcart or other mechanisms

### Table 4-4: Treasure

Most chaos wildlife does not have treasure but some may have some at their lair and a few may carry some on them. Choose a die on the dice chain to limit the treasure value.

d16

Treasure

d16

Treasure

1

Potion (d16 on _DCC RPG_ Pg. 446)

9

Useful herbs

2

Shiny bits, d20: 1. Ring -5 Luck, cannot be removed; 20. Emerald (100 gp).

10

1d100 gp

3

1d3 Rations

11

The journal of a fallen adventurer

4

2d4 Rations

12

Vial of 4 Rubies (50 gp each)

5

1d30 sp

13

Map of (roll for Doom Hex name)

6

1d10 gp

14

Strange writing (spell)

7

Dagger

15

Scroll (d100 on _DCC RPG_ Pg. 373)

8

Holy symbol (20 gp)

16

Potion (d20+15+Luck mod on _DCC RPG_ pg. 223)

### Table 4-5: Encounter Type

These beasts will typically have a signature introduction. The judge is invited to create a scene appropriate to the creature. _Gloom Worms_ attack at night. Randomly determine who is on watch and go from there. _Glam Elves_ appear as cute woodland faeries that eat you. The Fist time you meet a beast should be special. You can also use the table below liven up further encounters.

Roll d20 or d14+3 (less extreme) or 3d6 (favors center) +/- Luck Modifier.  
You could also d14 for trouble or d14+6 for an easy time or d10+5 for an ok time.

d20

Encounter

d20

Encounter

1

Doom

11

Attack

2

Surrounded

12

Your are tested

3

Hunted

13

A beast is surprised

4

In their territory

14

You see them first

5

Surprised

15

An NPC is under attack (choose from NPC table)

6

A trap

16

A beast is curious

7

Cornered

17

Beasts fighting

8

Attacked from behind

18

A beast does not see you

9

Difficult terrain

19

A beast needs help

10

Attack

20

A beast in the distance

### Beast Levels

These beasts are formulated for lower level characters. The beasts that inhabit a hex do not change as the characters gain experience. These lower level beasts simply avoid the PCs as they pass through. Further hexes may contain higher level beasts. Starting at level 3-4, Add +1 per additional PC level to the AC, to hit bonus, and saving throws. Add +d1 to damage. Add .5 per PC level to HD.

# 5\. Neutral Hex Encounters

Choose, skip, or roll on these table.

### Table 5-1: People and Parties

See _DCC RPG_ Appendix S and T for names and titles.

d30

Encounter

d30

Encounter

1

Plague victims

16

Magician

2

Acolytes

17

Man-at-Arms

3

Armed forces

18

Noble

4

Artisans

19

Peasant

5

Assassin

20

Pilgrims

6

Bandit

21

Chain gang

7

Berserker

22

Slavers

8

Caravan

23

Sage

9

Escapees

24

Traders

10

Fortune Teller

25

Victims

11

Friar

26

Witch (neutral)

12

Scout

27

Mysterious Traveller

13

King and entourage

28

Odd Merchant

14

Knight

29

Sooth Sayer

15

Lost people

30

0-levels looking for adventure

### Table 5-2: Settlements

Larger settlements will be planned but some smaller settlements may be come across randomly. Use a d14 if you do not want random villages or forts.

d16

Encounter

d16

Encounter

1

Farm

9

Dojo

2

Inn

10

Stronghold

3

Trading post

11

Temple

4

Mining colony

12

Colony of friendly creatures

5

Outpost

13

Compound

6

Spider Goat Herder

14

Schtetl

7

Nomads

15

Village

8

Gladiator training school

16

Small Fort

![](image/homes.svg)

# 6\. Death by Hex

Our story is not likely to end on a hex. Random events can multiply to create effects that are insurmountable. There are generally two ways to avoid a TPK in such situations. Run or rescue.

## Run

After disengaging from melee, each PC makes a DC 10 Reflex save to break away from the enemy. Failure means at least one enemy catches up with that PC. They can try again next round at DC 5 and be aided by missile fire or other means. A distraction may also help. If they fight, more of the enemy will catch up.

Faster enemies will still be on your tail. Some options are:

1.  Enter a new hex type. Beasts tend to not venture out of their home environment. PCs must make a DC 5 Reflex just at the hex's edge or trip.
2.  A ravine with a fallen tree across it. DC 5 Reflex to get across the log. Failure means luck check to hang from the log and an enemy may catch up. Failure means into the drink or die. Once you cross over, DC 15 Strength check to dislodge the tree or fight them as they come across.
3.  A cave offers some respite. Fight them at the mouth of the cave or something in the cave scares them away.
4.  Scramble up a cliff face. DC 5 Reflex or fall for (1d3-1)d6 damage and they are upon you.
5.  Jump off a cliff into a river that flows between hexes. DC 10 Reflex save or 1d3 damage. Wash up downriver.
6.  Swim a large river that flows between hexes. Lose anything heavy.
7.  Come upon a settlement **Table 5-2.** DC 5 reflex as the settlement comes into view.

## Rescue

Some situations will require rescue. It is difficult to outrun a field of _Dream Poppies._ You may be rescued right away or after waking up in the middle of the ritual. Your savior need not be beneficient. The _Evil Flying Monkeys_ that save you can take your sleeping bodies straight to the castle of the _Black Witch_. In addition to the options below, a player' patron could intercede on their behalf.

1.  Predator becomes prey. You hear a noise. A look of fear comes over the face of your enemy. A much larger beast comes through and chases your assailants away. You could roll up a Some Lizards or Bears from above.
    
2.  The Servants of, or a powerful NPC/creature saves you.
    
    1.  A chaotic master drags you out of the frying pan for
        
        1.  An experiment
        2.  A wedding (your own)
        3.  Trial by combat
        4.  Something you know or own
    2.  A neutral master comes to your aid
        
        1.  For a mission
        2.  For payment
        3.  Just because
    3.  A Lawful master rescues you
        
        1.  Because she watches over these lands.
        2.  Happened to be in the area
        3.  Knows that your fate is not complete
        4.  For a mission
3.  A scouting party of rangers
    
4.  Mounted, armed forces passing through
    
5.  Powerful monks
    
6.  Bandits who proceed to rob you
    
7.  A band of 0 levels looking for adventure
    

![](image/robin.svg)

# 7\. Psychogeography

Ley lines, songlines, or dream tracks, these telluric currents cross the earth in patterns without repetition. They spring from and back to the Umbilicus Mundi. A place without location. In regards to applied geomancy, their use is a guarded secret.

Players would not know that the lines can affect spell checks and not necessarily for the better. Points of intersection are lenses of disturbance. No one can say what the results of practice will be at these locations. The ancient huacas sometimes found there serve to temper or celebrate the chaos.

Points of focus are harder to locate. These may have been marked by subtle geoglyphs that point out the location from afar. Distant from the chaotic influence of telluric cross currents, they can increase a wizard's arcane powers.

The Judge should take care to add the psychogeographical modifiers to spell checks even if the players do not know why.

The psychogeographical map lies under the physical map and can only be discerned through testing (casting spells at particular locations) or through the _Divination_ spell.

You may keep track of ley lines on your Judges map. You could draw them in as you please or let it be random.

**Column** d9: 1 A, 2 B, 3 C, 4 D, 5 E, 6 F, 7 G, 8 H, 9 I  
**Direction** d3: 1 NW, 2 W, 3 SW  
**Occidental crosscurrent** d20-1: 0-19 hexes away  
**Oriental crosscurrent** d20-1: 0-19 hexes away

If any crossing ley line appears on the map, determine its direction as well and use a d100 for crosscurrents.

## Ley Line effects

Casting a spell on a Ley Line adds 1d3 to the spell check. Ley line intersections and focal points have additional effects.

### Ley Line Intersection

Intersections have a 50% (+Luck mod x 10) chance of having a positive or negative affect. Roll a d6, add or subtract it from the spell check and look it up on the following table (regardless of whether it is positive or negative). Add +1 for each additional Ley line at this intersection.

1.  Random spell misfires (lvl 1)
2.  2 random spells misfire (lvl 1 & 2)
3.  3 spells (lvl 1, 2, & 3)
4.  3 spells and phlogiston disturbance
5.  3 spells and phlogiston and Minor Corruption
6.  As above but Major corruption
7.  As above but Greater corruption
8.  As above and something is released (_DCC RPG_ Pg. 401)
9.  As above and hole is opened

### Ley Line Focal Point

Points of focus occur equidistant from intersections that are at least 4 hexes away in each direction. The intersections must be at least 9 hexes apart to have a focal point. Focal points add 3d5 to the spell check.

## Divination Spell

Level: 0, Range: 60' or more, Duration: 2 turns, Casting time: 2 actions

### General

A spell to divine the locations of ley lines or other geological features such as an underground water supply.

This is one of the simpler spells and is used by some of the more learned shaman or fortune tellers to find underground wells or mining locations. Wizards may learn this spell at first level as well as Clerics who follow gods of earth, nature, or secrets. When cast by a true magic user the spell’s benefits increase as do any ill effects.

The cleric or wizard must choose a target such as ley lines, an underground water source, an underground cavity, or a deposit of a particular mineral. The magic user must then concentrate for 2 actions to cast this spell. Additional concentration time adds +1 to the spell check.

If you are judging by the seat of your pants and wish to have a ley line nearby but have not determined the location of any ley lines, roll 1d10 (reroll 10s) to determine the column, d6 for row, and d3 for direction

**Manifestation** See Below

### 0-Level

Typically the caster walks seemingly randomly and, either senses the target below them or feels as if their divining rod suddenly points down.

A Divining Rod adds +1 and Dwarves receive a +2 to this spell check.

**1** The caster falsely senses a target below them at a random location.  
**2-8** No effect.  
**9+** If the caster passes over a target, she feels its presence.

A 0-level may only cast this spell once per week and typically use a d10. Casting rate and spell check may be affected by certain inborn talents, or rituals.

### Magic User

On a result of natural 1, a wizard suffers a 50% chance of major corruption or misfire, rolling on the generic tables as appropriate.

**1-11** Failure

**12-13** The caster can determine if there is a ley line in the current hex or an adjacent hex though not which adjacent hex. The caster can determine if the targeted geological feature is nearby or its general direction if it is in the same hex.

**14-17** The caster can determine if their are any ley lines in the current hex or surrounding 6 hexes and their direction. The caster can determine direction and distance of the targeted feature in this space.

**18-19** The caster can determine the locations of any ley lines up to two hexes out. If the current hex contains a ley line the caster can determine the location of the closest crosscurrent. The caster can determine the direction and distance of the targeted feature in this space.

**20-23** The caster can determine all ley lines within 3 hexes and can see all adjacent crosscurrents for any lines in the current hex. There is a chill in the air above the targeted features in this space.

**24-27** The caster can determine all ley lines within 3 hexes and can see all adjacent crosscurrents for all of those lines. Beautiful music seems to emanate from the ground in the location of all the target features within this space.

**28-29** The caster has a complete understanding of the ley lines of this map section. The caster understands how ley lines affect spell checks and where the points of focus are located. Birds and harmless animals romp and play above the targeted mineral or geological feature.

**30-31** The caster has a complete understanding of the ley lines of this map section and the adjacent 8 map sections. The caster understands how ley lines affect spell checks and where the points of focus are located. The targeted feature or minerals are highlighted to the eyes of the caster and anyone touching the caster by pillars of blue light that shine into the heavens for 2 weeks.

**32+** The caster knows all there is to know about ley lines and their locations worldwide. All large types of the geological feature or mineral become visible to the caster as the globe becomes transparent to the caster and anyone touching the caster. Even tiny amounts the mineral become apparent to the caster if it is close by.

# 8\. Resources

*   [http://juliosrpgcove.com/storegenerator](http://juliosrpgcove.com/storegenerator/)
*   Shopping items
*   Monster Manuals  
    [https://diyanddragons.blogspot.com/search/label/dcc](https://diyanddragons.blogspot.com/search/label/dcc)  
    [https://knightsinthenorth.blog/category/monster/](https://knightsinthenorth.blog/category/monster/)  
    [https://appendixm.blogspot.com](https://appendixm.blogspot.com/)  
    [https://ravencrowking.blogspot.com/2018/12/making-monsters-for-dungeon-crawl.html](https://ravencrowking.blogspot.com/2018/12/making-monsters-for-dungeon-crawl.html)
*   Town Generator
*   Inns
*   Mapping
*   Encounter Tables

![](image/weatherflower.svg)

![](coversketcho.svg) ![](image/GlowRatSplat.svg) ![](image/howler.svg)

_Rolls: Phase, Glam, Gore, Healer, Women_

(3d3): Init +2; Whip +5 melee, 10' reach (1d6 damage or DC 15 Ref disarm or DC 15 Str grapple); AC 12 (+4 if threatened); HD 1d7; MV 30’; Act 1d20 + 1d20 for tentacles; SV Fort +1, Ref +6, Will +2; AL C. Straight out of the male subconscious.

Green skin, long blue hair, and goat's eyes. They appear as lost and lonely women. Elves will know it is a glamour but must make a DC 10 spell check to see through (considered rude). Each carries an ox blood whip. When threatened they appear to blink in and out of existance (+4 AC) and unarmed attacks are out of phase. DC 15 Fort save or stun 1 round and ignore non-magical armor. When threatend piercing tentacles unsheath. Additional Act 1d20, +3 melee (1d4 damage + grapple). Grapple does 1d6 damage/turn and reduces AC until DC 15 Str check or tentacles are cut or withdrawn.

One member of the group is a Healer. She is the same but does not carry a weapon and AC 11, HD 2d7, and Fort +4. Any hag within 10' of the healer gains 2hp/round.

Displacer Hags